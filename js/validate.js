function showMessage(idSpan, message){
    document.getElementById(idSpan).innerHTML = message;
}

function kiemTraSo(code, idSpan){
    if (isNaN(code)) {
        showMessage(idSpan, "Vui lòng nhập số!");
      return false;
    } else {
        showMessage(idSpan, "");
        return true;
    }
}

function kiemTraTrung(taiKhoanNV, dsnv, idSpan){
    var index = dsnv.findIndex(function(item_dsnv){
        return item_dsnv.ma == taiKhoanNV;
    });

    //Nếu tìm được index thì chứng tỏ có mã trùng trong mảng => false => gọi hàm showMessage thông báo mã đã tồn tại 
    if (index == -1){
        showMessage(idSpan,"");
        return true;
    } else {
        showMessage(idSpan,"Tài khoản đã tồn tại!");
        return false;
    }
}

function kiemTraDoDai(min, max, idSpan, value, message){
    let length = value.length;
    if(length >= min && length <= max){
        showMessage(idSpan, "");
        return true;
    }else{
        showMessage(idSpan, message);
        return false;
    }
}

function kiemTraNull(item_array_dsnv, idSpan, message){
    if(item_array_dsnv != "") {
        showMessage(idSpan, "");
        return true;
    }else {
        showMessage(idSpan, message);
        return false;
    }
}

function kiemTraTen(nameNV, idSpan, message){
    const re = /^[A-Za-z\s]*$/;
    var isNameNv = re.test(nameNV);
    if (isNameNv){
        showMessage(idSpan, "");
        return true;
    }else {
        showMessage(idSpan, message);
        return false;
    }
}
function kiemTraEmail(email,idSpan){
    const re = 
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(email) ;// phương thức test trả giá trị true false, nếu email truyền vào thỏa re thì trả về true
  if(isEmail){
    showMessage(idSpan,"");
    return true;
  } else{
    showMessage(idSpan,"Email không hợp lệ");
    return false;
  }
}

function kiemTraDiem(score, idSpan, message) {
    // Kiểm tra xem giá trị nhập vào có phải là một số hay không
    if (isNaN(score)) {
        showMessage(idSpan, "Điểm phải là số!");
      return false;
    }
    
    // Kiểm tra xem điểm có nằm trong khoảng từ 0 đến 10 hay không
    if (score >= 0 && score <= 10) {
        showMessage(idSpan, "");
      return true;
    } else {
        showMessage(idSpan, message);
      return false;
    }
  }




function kiemTraluongCB(luongCB, idSpan){
    if(luongCB >= 100000 && luongCB <=400000){
        showMessage(idSpan, "");
        return true;
    }else{
        showMessage(idSpan, "Lương ngày từ 100000vnd đến 400000vnd")
    }
}

function kiemTraChucVu(chucVuNV){
    if (chucVuNV == "Sếp" || chucVuNV == "Trưởng phòng" || chucVuNV == "Nhân viên"){
        showMessage("tbChucVu","");
        return true;
    }else{
        showMessage("tbChucVu","Chức vụ không hợp lệ!");
        return false;
    }
}

function kiemTraGioLam(gioLam){
    if(gioLam >= 80 && gioLam <= 200){
        showMessage("tbGiolam", "");
        return true;
    }else{
        showMessage("tbGiolam", "Số giờ làm phải từ 80 đến 200 giờ!");
        return false;
    }
}
