

//Lấy thông tin người dùng nhập vào từ form
function layThongTinTuFormStudent(){
    let ma = document.getElementById("maStudent").value;
    let hoTen = document.getElementById("hoTenStudent").value;
    let diaChi = document.getElementById("diaChiStudent").value;
    let email = document.getElementById("emailStudent").value;
    let toan = document.getElementById("diemToan").value*1;
    let ly = document.getElementById("diemLy").value*1;
    let hoa = document.getElementById("diemHoa").value*1;
    let type = document.getElementById("customCheck1").value;
    return new Student(ma, hoTen, diaChi, email, toan, ly, hoa, type);
}

function layThongTinTuFormEmployee(){
    let ma = document.getElementById("maEmployee").value;
    let hoTen = document.getElementById("hoTenEmployee").value;
    let diaChi = document.getElementById("diaChiEmployee").value;
    let email = document.getElementById("emailEmployee").value;
    let soNgayLV = document.getElementById("soNgayLV").value;
    let luongNgay = document.getElementById("luongNgay").value*1;
    let type = document.getElementById("customCheck2").value;
    return new Employee(ma, hoTen, diaChi, email, soNgayLV, luongNgay, type);
}

function layThongTinTuFormCustomer(){
    let ma = document.getElementById("maCustomer").value;
    let hoTen = document.getElementById("hoTenCustomer").value;
    let diaChi = document.getElementById("diaChiCustomer").value;
    let email = document.getElementById("emailCustomer").value;
    let tenCongTy = document.getElementById("tenCongTy").value;
    let hoaDon = document.getElementById("hoaDon").value*1;
    let danhGia = document.getElementById("danhGia").value;
    let type = document.getElementById("customCheck3").value;
    return new Customer(ma, hoTen, diaChi, email, tenCongTy, hoaDon, danhGia, type);
}

//Render dữ liệu ra màn hình với Person là mảng các phần tử
function renderListPerson(Person){
    let contentHTML = "";
    for(let i = 0; i < Person.length; i++){
        if(Person[i].type === "Student"){
            let item = Person[i];
            let content = `
            <div class="card border-primary mb-3 ml-5" style="max-width: 25rem; width: 350px">
            <div class="card-header bg-primary" style="font-size: 20px">Student</div>
            <div class="card-body text-primary text-left">
                <p class="card-text">Mã học viên: ${item.ma}</p>
                <p class="card-text">Họ tên: ${item.hoTen}</p>
                <p class="card-text">Địa chỉ: ${item.diaChi}</p>
                <p class="card-text">Email: ${item.email}</p>
                <p class="card-text">Điểm toán: ${item.toan}</p>
                <p class="card-text">Điểm lý: ${item.ly}</p>
                <p class="card-text">Điểm hóa: ${item.hoa}</p>
                <p class="card-text">Điểm trung bình: ${item.tinhDTB()}</p>
            </div>
            <div class="card-footer bg-info">
                 <button class="btn btn-danger mr-1" onclick="deleteStudent(${item.ma})">Xóa</button>
                <button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="settingStudent(${item.ma})">Sửa</button>
            </div>
         </div>
            `;
        contentHTML = contentHTML + content;
        } else if ((Person[i].type === "Employee")) {
            let item = Person[i];
            let content = `
            <div class="card border-success mb-3 ml-5" style="max-width: 25rem; width: 350px">
            <div class="card-header bg-success" style="font-size: 20px">Employee</div>
            <div class="card-body text-left">
                <p class="card-text">Mã nhân viên: ${item.ma}</p>
                <p class="card-text">Họ tên: ${item.hoTen}</p>
                <p class="card-text">Địa chỉ: ${item.diaChi}</p>
                <p class="card-text">Email: ${item.email}</p>
                <p class="card-text">Số ngày làm việc: ${item.soNgayLV}</p>
                <p class="card-text">Lương ngày: ${item.luongNgay}</p>
                <p class="card-text">Tổng lương: ${item.tinhLuong()}</p>
            </div>
            <div class="card-footer" style="background-color: #669900">
                 <button class="btn btn-danger mr-1" onclick="deleteEmployee(${item.ma})">Xóa</button>
                <button class="btn btn-warning" data-toggle="modal" data-target="#myModalEmployee" onclick="settingEmployee(${item.ma})">Sửa</button>
            </div>
         </div>
            `;
        contentHTML = contentHTML + content;
        } else {
            let item = Person[i];
            let content = `
            <div class="card border-warning mb-3 ml-5" style="max-width: 25rem; width: 350px">
            <div class="card-header bg-warning" style="font-size: 20px">Customer</div>
            <div class="card-body text-left">
                <p class="card-text">Mã khách hàng: ${item.ma}</p>
                <p class="card-text">Họ tên: ${item.hoTen}</p>
                <p class="card-text">Địa chỉ: ${item.diaChi}</p>
                <p class="card-text">Email: ${item.email}</p>
                <p class="card-text">Tên công ty: ${item.tenCongTy}</p>
                <p class="card-text">Trị giá hóa đơn: ${item.hoaDon}</p>
                <p class="card-text">Đánh giá: ${item.danhGia}</p>
            </div>
            <div class="card-footer" style="background-color:  #CD9B1D">
                 <button class="btn btn-danger mr-1" onclick="deleteCustomer(${item.ma})">Xóa</button>
                <button class="btn btn-warning" data-toggle="modal" data-target="#myModalCustomer" onclick="settingCustomer(${item.ma})">Sửa</button>
            </div>
         </div>
            `;
        contentHTML = contentHTML + content;
        }
        }
        document.getElementById("contentRender").innerHTML = contentHTML;
    }
 
       

//Show thông tin từ mảng chứa các person lấy ra thẻ student có thuộc tính mã trùng với mã được truyền vào từ nút settting 
function showThongTinLenFormStudent(student_listperson){
    document.getElementById("maStudent").value = student_listperson.ma;
    document.getElementById("hoTenStudent").value = student_listperson.hoTen;
    document.getElementById("diaChiStudent").value = student_listperson.diaChi;
    document.getElementById("emailStudent").value = student_listperson.email;
    document.getElementById("diemToan").value = student_listperson.toan;
    document.getElementById("diemLy").value = student_listperson.ly;
    document.getElementById("diemHoa").value = student_listperson.hoa;
    document.getElementById("customCheck1").value = student_listperson.type;
}

//Show thông tin Employee
function showThongTinLenFormEmployee(employee_listperson){
    document.getElementById("maEmployee").value = employee_listperson.ma;
    document.getElementById("hoTenEmployee").value = employee_listperson.hoTen;
    document.getElementById("diaChiEmployee").value = employee_listperson.diaChi;
    document.getElementById("emailEmployee").value = employee_listperson.email;
    document.getElementById("soNgayLV").value = employee_listperson.soNgayLV;
    document.getElementById("luongNgay").value = employee_listperson.luongNgay;
    document.getElementById("customCheck2").value = employee_listperson.type;
    
}

//Show thông tin Customer
function showThongTinLenFormCustomer(customer_listperson){
    document.getElementById("maCustomer").value = customer_listperson.ma;
    document.getElementById("hoTenCustomer").value = customer_listperson.hoTen;
    document.getElementById("diaChiCustomer").value = customer_listperson.diaChi;
    document.getElementById("emailCustomer").value = customer_listperson.email;
    document.getElementById("tenCongTy").value = customer_listperson.tenCongTy;
    document.getElementById("hoaDon").value = customer_listperson.hoaDon;
    document.getElementById("danhGia").value = customer_listperson.danhGia;
    document.getElementById("customCheck3").value = customer_listperson.type;
}


