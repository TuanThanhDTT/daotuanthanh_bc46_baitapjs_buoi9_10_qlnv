class Person {
    constructor(ma, hoTen, diaChi, email)
    {
        this.ma = ma;
        this.hoTen = hoTen;
        this.diaChi = diaChi;
        this.email = email;
    }
}

class Student extends Person {
    constructor(ma, hoTen, diaChi, email, toan, ly, hoa, type)
    {
        super(ma, hoTen, diaChi, email);
        this.toan = toan;
        this.ly = ly;
        this.hoa = hoa;
        this.dtb = 0;
        this.type = type;
    }
    tinhDTB(){
        return this.dtb = (this.toan + this.ly +  this.hoa) / 3;
    }
}

class Employee extends Person {
    constructor(ma, hoTen, diaChi, email, soNgayLV, luongNgay, type)
    {
        super(ma, hoTen, diaChi, email);
        this.soNgayLV = soNgayLV;
        this.luongNgay = luongNgay;
        this.tongLuong = 0;
        this.type = type;
    }
    tinhLuong(){
        return this.tongLuong = this.soNgayLV * this.luongNgay;
    }
}

class Customer extends Person {
    constructor(ma, hoTen, diaChi, email, tenCongTy, hoaDon, danhGia, type)
    {
        super(ma, hoTen, diaChi, email);
        this.tenCongTy = tenCongTy;
        this.hoaDon = hoaDon;
        this.danhGia = danhGia;
        this.type = type;
    }
}

class ListPerson {
    constructor() {
      this.persons = [];
    }
  
    // Thêm một đối tượng vào danh sách
    addPerson(person) {
      this.persons.push(person);
    }
  
    // Lấy danh sách các đối tượng
    getPersons() {
      return this.persons;
    }
  }


